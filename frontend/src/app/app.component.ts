import { Component, ElementRef, ViewChild } from '@angular/core';
import { TimetableModel } from './models/timetableModel';
import { TimetableService } from './services/timetableService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('form1') form1: any;
  timeTableVM = new TimetableModel();
  timeTableList: any[] = [];
  showTable: boolean = false;
  submitted: boolean = false;
  loading: boolean = false;

  constructor(private timetableService: TimetableService) {

  }

  checkValidation() {
    let msg = "";
    if (this.timeTableVM.noWorkingDays != undefined && this.timeTableVM.noSubjectsPerDay != undefined && this.timeTableVM.totalSubjects != undefined) {
      if (Number(this.timeTableVM.noWorkingDays) > 7) {
        this.submitted = true;
        alert("Please enter only +ve number between 1 to 7");
      }
      else if (Number(this.timeTableVM.noSubjectsPerDay) > 9) {
        this.submitted = true;
        alert("Please enter only +ve number between 1 to 9");
      }
      else if (this.timeTableVM.subjectsNames != undefined && this.timeTableVM.subjectsNames.length > 0) {
        this.timeTableVM.subjectsNames.forEach(subject => {
          if (subject == "") {
            this.submitted = true;
            msg = "Please enter all required subject(s)";
          }
        });
        if (this.submitted)
          alert(msg);
      }
      else {
        this.timeTableVM.totalHourWeek = Number(this.timeTableVM.noWorkingDays) * Number(this.timeTableVM.noSubjectsPerDay);
      }
    }
    else {
      this.submitted = true;
    }
  }

  onHandleInput() {
    this.submitted = false;
    this.checkValidation();
  }

  addSubjects(length: number) {
    if (length) {
      if (length > this.timeTableVM.subjectsNames.length) {
        for (let i = this.timeTableVM.subjectsNames.length; i < length; i++) {
          this.timeTableVM.subjectsNames.push("");
        }
      } else {
        this.timeTableVM.subjectsNames.splice(length, this.timeTableVM.subjectsNames.length - length);
      }
    }
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  onSubmit() {
    this.submitted = false;
    this.checkValidation();
    if (this.form1.valid && !this.submitted) {
      this.loading = true;
      this.timetableService.fetchTimeTable(this.timeTableVM).subscribe(res => {
        if (res) {
          this.showTable = true;
          this.timeTableList = res;
          this.loading = false;
        }
      })
    }
  }

}
