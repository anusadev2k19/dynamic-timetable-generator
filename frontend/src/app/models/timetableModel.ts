export class TimetableModel {
    noWorkingDays: any | undefined;
    noSubjectsPerDay: any | undefined;
    totalSubjects: any | undefined;
    totalHourWeek: any | undefined;
    subjectsNames: Array<{}> = [];
}