import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-timetable',
    templateUrl: './timetable.component.html'
})
export class TimetableComponent {
    @Input() timeTableList: any[] = [];
    constructor() {
    }

}
