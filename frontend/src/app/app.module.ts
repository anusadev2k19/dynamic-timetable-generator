import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SpinnersAngularModule } from 'spinners-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimetableComponent } from './timetable.component';
import { TimetableService } from './services/timetableService';

@NgModule({
  declarations: [
    AppComponent, TimetableComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, SpinnersAngularModule,
    AppRoutingModule
  ],
  providers: [TimetableService],
  bootstrap: [AppComponent]
})
export class AppModule { }
