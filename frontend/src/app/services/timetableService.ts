import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TimetableService {
    baseURL: string = "https://localhost:44358/";

    constructor(private http: HttpClient) { }

    fetchTimeTable(timeData: any): Observable<any> {
        const headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }
        const body = JSON.stringify(timeData);
        return this.http.post(this.baseURL + 'timetable', body, headers)
    }
}  