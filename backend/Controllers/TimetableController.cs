﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Timetable.Models;

namespace Timetable.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [EnableCors("Cors")]
    public class TimetableController : ControllerBase
    {
        private Random _rand = new Random();

        [HttpPost]
        public async Task<ActionResult<TimetableRequest>> FetchTimeTable(TimetableRequest timetableReq)
        {
            var timetableList = new List<TimetableResponse>();
            if (timetableReq != null)
            {
                for (int j = 1; j <= timetableReq.NoSubjectsPerDay; j++)
                {
                    var timetableResponse = new TimetableResponse();
                    timetableResponse.Row = j;

                    for (int i = 1; i <= timetableReq.NoWorkingDays; i++)
                    {
                        timetableResponse.Column = i;
                        timetableResponse.Subject = new List<string>();

                        var SubjectLists = timetableReq.SubjectsNames.OrderBy(_ => _rand.Next()).ToList();

                        timetableResponse.Subject.AddRange(SubjectLists);

                        var totRemainCol = Convert.ToInt32(timetableReq.NoWorkingDays) - Convert.ToInt32(timetableReq.NoSubjectsPerDay);

                        for (int k = 1; k <= totRemainCol; k++)
                        {
                            timetableResponse.Subject.Add(SubjectLists.OrderBy(_ => _rand.Next()).FirstOrDefault());
                        }
                    }

                    timetableList.Add(timetableResponse);
                }
            }

            return new JsonResult(timetableList);
        }
    }
}
