﻿using System.Collections.Generic;
using System.Data;

namespace Timetable.Models
{
    public class TimetableResponse
    {
        public int Column { get; set; }
        public int Row { get; set; }
        public List<string> Subject { get; set; }
    }
}
