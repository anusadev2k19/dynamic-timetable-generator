﻿using System.Collections.Generic;

namespace Timetable.Models
{
    public class TimetableRequest
    {
        public int NoWorkingDays { get; set; }
        public int NoSubjectsPerDay { get; set; }
        public int TotalSubjects { get; set; }
        public int TotalHourWeek { get; set; }
        public List<string> SubjectsNames { get; set; }

    }
}
